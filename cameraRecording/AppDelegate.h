//
//  AppDelegate.h
//  cameraRecording
//
//  Created by Robert_Hsu on 2018/1/27.
//  Copyright © 2018年 testOC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

