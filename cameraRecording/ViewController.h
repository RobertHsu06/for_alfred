//
//  ViewController.h
//  cameraRecording
//
//  Created by Robert_Hsu on 2018/1/27.
//  Copyright © 2018年 testOC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <OpenGLES/gltypes.h>
#import <AVKit/AVKit.h>
@interface ViewController : UIViewController<AVCaptureVideoDataOutputSampleBufferDelegate>

@property (nonatomic, strong) AVCaptureSession *avCapSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *avPreViewLayer;
@property (nonatomic, strong) AVCaptureDevice *capDevice;
@property (nonatomic, strong) AVAssetWriter *avWriter;
@property (nonatomic, strong) AVAssetWriterInput *avWriterInput;
@property (nonatomic, assign) size_t bytesPerRow;

@property (nonatomic, assign) size_t width;
@property (nonatomic, assign) size_t height;

@property (nonatomic) BOOL isRecording;
@property dispatch_queue_t captureSessionQueue;
@property (weak, nonatomic) IBOutlet UIButton *btnRecording;
@property (weak, nonatomic) IBOutlet UIView *preVideoPreview;
- (IBAction)tapToFocus:(UITapGestureRecognizer *)sender;
- (IBAction)startOrStopRecording:(id)sender;
- (IBAction)onPlayback:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnplayback;
@end

