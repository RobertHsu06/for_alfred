//
//  ViewController.m
//  cameraRecording
//
//  Created by Robert_Hsu on 2018/1/27.
//  Copyright © 2018年 testOC. All rights reserved.
//

#import "ViewController.h"


typedef void(^PropertyChangeBlock)(AVCaptureDevice *captureDevice);

@interface ViewController ()
@property (nonatomic, strong) dispatch_queue_t videoQueue;

@property (strong, nonatomic) AVCaptureSession *captureSession;

@property (nonatomic, strong) AVCaptureDeviceInput *videoInput;
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoOutput;

@property (nonatomic, strong) AVAssetWriter *assetWriter;
@property (nonatomic, strong) AVAssetWriterInput *assetWriterVideoInput;
@property (nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor *adaptor;
@property (nonatomic, strong) NSDictionary *videoCompressionSettings;
@property (nonatomic, assign) BOOL canWrite;

@property (strong, nonatomic) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (strong, nonatomic) NSURL *videoURL;
@property (assign, nonatomic) UIDeviceOrientation shootingOrientation;                 
@property (nonatomic, strong) CIContext *context;
@end

@implementation ViewController
@synthesize avPreViewLayer;
@synthesize capDevice;
@synthesize avWriter;
@synthesize avWriterInput;
@synthesize isRecording;
@synthesize avCapSession;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.context = [CIContext context];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    [self setupAVCapture];
}

- (void) viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidAppear:(BOOL)animated{
    if (![self.captureSession isRunning])
    {
        [self.captureSession startRunning];
    }
}

- (void) viewDidDisappear:(BOOL)animated{
    if ([self.captureSession isRunning])
    {
        [self.captureSession stopRunning];
    }
}

-(BOOL) shouldAutorotate{
    return !self.isRecording;
}

- (void) orientationChanged:(NSNotification *)note
{
    self.avPreViewLayer.frame = self.view.bounds;
    UIDevice * device = note.object;
    AVCaptureVideoOrientation currentRotation = AVCaptureVideoOrientationPortrait;

    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            /* start special animation */
            currentRotation = AVCaptureVideoOrientationPortrait;
            break;

        case UIDeviceOrientationPortraitUpsideDown:
            /* start special animation */
            currentRotation = AVCaptureVideoOrientationPortraitUpsideDown;
            break;
        case UIDeviceOrientationLandscapeLeft:
            currentRotation = AVCaptureVideoOrientationLandscapeRight;
            break;
        case UIDeviceOrientationLandscapeRight:
            currentRotation = AVCaptureVideoOrientationLandscapeLeft;
            break;
        default:
            break;
    };
    self.shootingOrientation = device.orientation;
    self.avPreViewLayer.connection.videoOrientation = currentRotation;
}

#pragma mark - init members
- (AVCaptureSession *)captureSession
{
    if (_captureSession == nil)
    {
        _captureSession = [[AVCaptureSession alloc] init];
        
        if ([_captureSession canSetSessionPreset:AVCaptureSessionPreset1920x1080])
        {
            _captureSession.sessionPreset = AVCaptureSessionPreset1920x1080;
        }
    }
    
    return _captureSession;
}

- (dispatch_queue_t)videoQueue
{
    if (!_videoQueue)
    {
        _videoQueue = dispatch_get_main_queue();
    }
    
    return _videoQueue;
}

#pragma mark UIAction
- (IBAction)tapToFocus:(UITapGestureRecognizer *)sender {
    CGPoint thisFocusPoint = [sender locationInView:self.view];
    CGPoint pointOfInterest = CGPointMake(.5f, .5f);
    NSLog(@"taplocation x = %f y = %f", thisFocusPoint.x, thisFocusPoint.y);
    CGSize frameSize = self.view.frame.size;
    pointOfInterest = CGPointMake(thisFocusPoint.y / frameSize.height, 1.f - (thisFocusPoint.x / frameSize.width));
    

    
    NSError *error;
    
    [self.capDevice lockForConfiguration:&error];
    [self.capDevice setFocusPointOfInterest:CGPointMake(pointOfInterest.x,pointOfInterest.y)];
    [self.capDevice setFocusMode:AVCaptureFocusModeAutoFocus];
    if([self.capDevice isExposurePointOfInterestSupported] && [self.capDevice isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]){
        [self.capDevice setExposurePointOfInterest:pointOfInterest];
        [self.capDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
    }
    [self.capDevice unlockForConfiguration];

}

- (IBAction)startOrStopRecording:(id)sender {
    if(((UIButton*)sender).tag == 0){
        ((UIButton*)sender).tag = 1;
        [self.btnRecording setTitle:@"Stop" forState:UIControlStateNormal];
        if(self.videoURL != nil){
            self.videoURL = nil;
        }
        [self setupAVAssetWriterWithVideoWidth:_width height:_height];
        self.isRecording = YES;
        [self.btnplayback setEnabled:NO];
    }
    else{
        ((UIButton*)sender).tag = 0;
        [self.btnRecording setTitle:@"Start" forState:UIControlStateNormal];
        self.isRecording = NO;
        
        __weak __typeof(self)weakSelf = self;
        if(_assetWriter && _assetWriter.status == AVAssetWriterStatusWriting)
        {
            [self.btnplayback setEnabled:YES];
            [_assetWriter finishWritingWithCompletionHandler:^{
                weakSelf.canWrite = NO;
                weakSelf.assetWriter = nil;
                weakSelf.assetWriterVideoInput = nil;
                weakSelf.adaptor = nil;
            }];
        }
    }
}

- (IBAction)onPlayback:(id)sender {
    if(self.videoURL != nil){
        AVPlayer *player = [AVPlayer playerWithURL:self.videoURL];
        
        // create a player view controller
        AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
        [self presentViewController:controller animated:YES completion:nil];
        controller.player = player;
        [player play];
    }
}

- (void) setupAVCapture{
    if(self.videoInput != nil){
        return;
    }
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if (!captureDevice)
    {
        NSLog(@"get capture device failed");
        
        return;
    }
    
    NSError *error = nil;
    self.videoInput = [[AVCaptureDeviceInput alloc] initWithDevice:captureDevice error:&error];
    if (error)
    {
        NSLog(@"get video input device failed with reason：%@", error.localizedDescription);
        
        return;
    }
    
    
    if ([self.captureSession canAddInput:self.videoInput])
    {
        [self.captureSession addInput:self.videoInput];
    }
    
    self.videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    self.videoOutput.alwaysDiscardsLateVideoFrames = YES;
    [self.videoOutput setSampleBufferDelegate:self queue:self.videoQueue];
    if ([self.captureSession canAddOutput:self.videoOutput])
    {
        [self.captureSession addOutput:self.videoOutput];
    }
    
    self.avPreViewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    self.avPreViewLayer.frame = self.view.bounds;
    self.avPreViewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.preVideoPreview.layer addSublayer:self.avPreViewLayer];
}

- (CVPixelBufferRef) pixelBufferFromCGImage: (CGImageRef) image andSize:(CGSize) size
{
    size = CGSizeMake(CGImageGetWidth(image),
                                  CGImageGetHeight(image));
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVPixelBufferRef pxbuffer = NULL;
    
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, size.width,
                                          size.height, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef) options,
                                          &pxbuffer);
    NSParameterAssert(status == kCVReturnSuccess && pxbuffer != NULL);
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    NSParameterAssert(pxdata != NULL);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, size.width,
                                                 size.height, 8, 4*size.width, rgbColorSpace,
                                                 kCGImageAlphaNoneSkipFirst);
    NSParameterAssert(context);
    CGContextConcatCTM(context, CGAffineTransformMakeRotation(0));
    CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(image),
                                           CGImageGetHeight(image)), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    
    return pxbuffer;
}

- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection{
    
    if(_width == 0){//get video width/height
        
        CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        // Lock the base address of the pixel buffer
        CVPixelBufferLockBaseAddress(imageBuffer, 0);
        
        _bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
        // Get the pixel buffer width and height
        _width = CVPixelBufferGetWidth(imageBuffer);
        _height = CVPixelBufferGetHeight(imageBuffer);
        NSLog(@"bytes perrow = %zu, width = %zu, height = %zu",_bytesPerRow, _width,_height);
        CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    }
    
    if (sampleBuffer == NULL || self.isRecording == NO)
    {
        return;
    }
    
    @autoreleasepool
    {
        if (!self.canWrite)
        {
            [self.assetWriter startWriting];
            [self.assetWriter startSessionAtSourceTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
            self.canWrite = YES;
        }

        if (self.assetWriterVideoInput.readyForMoreMediaData)
        {
            // Image processing
            CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
            CIImage *sourceImage = [CIImage imageWithCVPixelBuffer:(CVPixelBufferRef)imageBuffer options:nil];
            CGRect sourceExtent = sourceImage.extent;

            // Image processing
            CIFilter * vignetteFilter = [CIFilter filterWithName:@"CIVignetteEffect"];
            [vignetteFilter setValue:sourceImage forKey:kCIInputImageKey];
            [vignetteFilter setValue:[CIVector vectorWithX:sourceExtent.size.width/2 Y:sourceExtent.size.height/2] forKey:kCIInputCenterKey];
            [vignetteFilter setValue:@(sourceExtent.size.width/2) forKey:kCIInputRadiusKey];
            CIImage *filteredImage = [vignetteFilter outputImage];

            CIFilter *effectFilter = [CIFilter filterWithName:@"CIPhotoEffectInstant"];
            [effectFilter setValue:filteredImage forKey:kCIInputImageKey];
            filteredImage = [effectFilter outputImage];
            
            CGImageRef cgImage = [self.context createCGImage:filteredImage fromRect:[filteredImage extent]];
            //Write some samples:
            CVPixelBufferRef buffer = NULL;
            buffer = [self pixelBufferFromCGImage:cgImage andSize:CGSizeZero];
            
            //with filter effect
            BOOL success = [self.adaptor appendPixelBuffer:buffer withPresentationTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
            
            //without filter
//                BOOL success = [self.adaptor appendPixelBuffer:imageBuffer withPresentationTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
            NSLog(@"success = %@",success ? @"YES" : @"NO");
             CGImageRelease(cgImage);
            CVPixelBufferRelease(buffer);
        }
    }
}

- (NSString *)getVideoCachePath {
    NSString *videoCache = [NSTemporaryDirectory() stringByAppendingPathComponent:@"videos"] ;
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:videoCache isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) ) {
        [fileManager createDirectoryAtPath:videoCache withIntermediateDirectories:YES attributes:nil error:nil];
    };
    return videoCache;
}

- (NSString *)getUploadFile_type:(NSString *)type fileType:(NSString *)fileType {
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HHmmss"];
    NSDate * NowDate = [NSDate dateWithTimeIntervalSince1970:now];
    ;
    NSString * timeStr = [formatter stringFromDate:NowDate];
    NSString *fileName = [NSString stringWithFormat:@"%@_%@.%@",type,timeStr,fileType];
    return fileName;
}

-(void) setupAVAssetWriterWithVideoWidth:(NSInteger) videoWidth height:(NSInteger) videoHeight{
    if (self.videoURL == nil)
    {
        NSString *videoPath = [NSString stringWithFormat:@"%@/%@",[self getVideoCachePath], [self getUploadFile_type:@"video" fileType:@"mp4"]];
            self.videoURL =[NSURL fileURLWithPath:videoPath];
    }
    
    self.assetWriter = [AVAssetWriter assetWriterWithURL:self.videoURL fileType:AVFileTypeMPEG4 error:nil];
    
    NSInteger numPixels = videoWidth * videoHeight;
    
    CGFloat bitsPerPixel = 12.0;
    NSInteger bitsPerSecond = numPixels * bitsPerPixel;
    
    NSDictionary *compressionProperties = @{ AVVideoAverageBitRateKey : @(bitsPerSecond),
                                             AVVideoExpectedSourceFrameRateKey : @(30),
                                             AVVideoMaxKeyFrameIntervalKey : @(30),
                                             AVVideoProfileLevelKey : AVVideoProfileLevelH264BaselineAutoLevel };
    
    self.videoCompressionSettings = @{ AVVideoCodecKey : AVVideoCodecH264,
                                       AVVideoScalingModeKey : AVVideoScalingModeResizeAspect,
                                       AVVideoWidthKey : @(videoHeight ),
                                       AVVideoHeightKey : @(videoWidth),
                                       AVVideoCompressionPropertiesKey : compressionProperties };
    
    self.assetWriterVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:self.videoCompressionSettings];
    
    self.assetWriterVideoInput.expectsMediaDataInRealTime = YES;
    
    self.adaptor =[AVAssetWriterInputPixelBufferAdaptor
                   assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.assetWriterVideoInput
                   sourcePixelBufferAttributes:nil];
    
    if ([self.assetWriter canAddInput:self.assetWriterVideoInput])
    {
        [self.assetWriter addInput:self.assetWriterVideoInput];
    }
    else
    {
        NSLog(@"AssetWriter videoInput append Failed");
    }
    
    _canWrite = NO;
    if (self.shootingOrientation == UIDeviceOrientationLandscapeRight)
    {
        self.assetWriterVideoInput.transform = CGAffineTransformMakeRotation(M_PI);
    }
    else if (self.shootingOrientation == UIDeviceOrientationLandscapeLeft)
    {
        self.assetWriterVideoInput.transform = CGAffineTransformMakeRotation(0);
    }
    else if (self.shootingOrientation == UIDeviceOrientationPortraitUpsideDown)
    {
        self.assetWriterVideoInput.transform = CGAffineTransformMakeRotation(M_PI + (M_PI / 2.0));
    }
    else
    {
        self.assetWriterVideoInput.transform = CGAffineTransformMakeRotation(M_PI / 2.0);
    }
}
@end
